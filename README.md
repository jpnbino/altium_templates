# Altium Template Project

Project contains templates for [**Altium Designer Software**](https://www.altium.com/), but can be updated to any PCB design software (Take look at the pdf if you don't have Altium installed or download the trial version for free). The templates can be used to facilitate project organization and documentation for a single person or a work team.

---

## Which are the templates?

Various templates are covered.

* Project : Defines how project sources, pages, parameters and notations should be organized.
* Stamp : Describes a page model for each schematic page. ( the archive name ending in ENG stands for english and PT for portuguese).
* BOM : Describes a template for a BOM list which is automatically fulfilled when BOM is generated through software.

---

## Who do I talk to

For any question or suggestion send me an email: jpnogg@gmail.com

---

## May I use it ?

Yes, feel free to update to your needs.